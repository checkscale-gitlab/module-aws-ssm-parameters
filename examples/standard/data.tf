data "aws_caller_identity" "this" {}

data "aws_iam_policy_document" "kms_policy" {
  statement {
    effect  = "Allow"
    actions = ["kms:*"]
    resources = [
      "*"
    ]
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    condition {
      test     = "StringLike"
      variable = "aws:userId"
      values = distinct(concat(
        ["${split(":", data.aws_caller_identity.this.user_id)[0]}*"]
      ))
    }
  }
}
